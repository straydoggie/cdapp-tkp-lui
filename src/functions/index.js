function getSequance(ceiling, random) {
   var seq = [];
   if (random) {
      while (seq.length < ceiling) {
         var num = Math.floor(Math.random() * ceiling);
         var ok = true;
         for (let i = 0; i < seq.length; i++) {
            if (seq[i] === num) {
               ok = false;
               break;
            }
         }
         if (ok) seq.push(num);
      }
   } else {
      for (let x = 0; x < ceiling; x++) {
         seq.push(x);
      }
   }

   return seq;
}


export {
   getSequance
} 