import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { getSequance } from '@/functions/index.js'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    title: "西南空管局党员党风廉政测试题",
    footerTip: " 制作 成都终端管制室 2019年4月",
    choiceNotice: true,
    isNumberRandom: true,
    isOptionRandom: true,
    isTimeLimited: false,
    timeLimit: 1200,
    timeUsed: 0,
    dateSetFrom: [2019, 4, 12],
    dateSetTo: [2019, 4, 15],
    userName: "",
    content: null,
    sequence: null,
    recorder: [],
    /*
    {
      identifier:
      content:[]
    }
    */
    pointer: 0,
    submitted: false,
    score: 0,
    style: {
      noticeBadge: "bg-danger text-white notice-badge",
      optionChosed: "bg-danger text-white",
      navbar: "navbar navbar-expand-xl bg-danger navbar-dark"
    }
  },
  mutations: {
    init(state, data) {
      if (data && data.length > 0) {
        state.content = data;
        state.sequence = getSequance(data.length, state.isNumberRandom);
      }
      //window.console.log(state.sequence);
    },
    next(state) {
      if (state.pointer < state.sequence.length - 1) {
        state.pointer++;
      } else {
        state.pointer = 0
      }
    },
    prev(state) {
      if (state.pointer > 0) {
        state.pointer--;
      } else {
        state.pointer = state.sequence.length - 1
      }
    },
    update(state, detail) {
      let done = false;
      for (let i = 0; i < state.recorder.length; i++) {
        if (state.recorder[i].identifier === detail.identifier) {
          done = true;
          break;
        }
        if (!done) {
          state.recorder.push(detail);
        }
      }
    },
    cleanChoice(state, identifier) {
      for (let i = 0; i < state.recorder.length; i++) {
        if (state.recorder[i].identifier == identifier) {
          state.recorder[i].content = [];
        }
      }

    },
    mekeChoice(state, choice) {
      /*
      choice: {
        identifier: "",
        value: 0
      }
      */
      let existed = false;
      for (let i = 0; i < state.recorder.length; i++) {
        if (state.recorder[i].identifier == choice.identifier) {
          existed = true;
          let chosed = false;
          for (let j = 0; j < state.recorder[i].content.length; j++) {
            if (state.recorder[i].content[j] == choice.value) {
              chosed = true;
              state.recorder[i].content.splice(j, 1);
            }
          }
          if (!chosed) {
            state.recorder[i].content.push(choice.value);
          }
        }
      }
      if (!existed) {
        state.recorder.push({
          identifier: choice.identifier,
          content: [choice.value]
        });
      }
      //window.console.log(state.recorder);
    },
    setUserName(state, value) {
      state.userName = value;
    },
    submit(state) {
      state.submitted = true;
    },
    setScore(state, value) {
      state.score = value;
    },
    timeDown(state) {
      state.timeUsed++;
    }
  },
  actions: {
    initContent(context) {
      axios
        .get('./data.json')
        .then(response => context.commit("init", response.data))
        .catch(error => window.console.log(error));
    },
    submit(context) {
      context.commit("submit");
      let score = 0;
      let detail = [];
      for (let i = 0; i < context.state.content.length; i++) {
        let c = context.state.content[i];
        let right = false;
        let obj = {
          id: c.identifier,
          //content: c.content,
          answers: c.answers,
          record: [],
          right: false
        }
        for (let j = 0; j < context.state.recorder.length; j++) {
          if (context.state.content[i].identifier == context.state.recorder[j].identifier) {
            let r = context.state.recorder[j];
            obj.record = r.content;
            if (c.answers.length != r.content.length) {
              right = false;
            }
            else {
              right = true;
              for (let x = 0; x < c.answers.length; x++) {
                let existed = false;
                for (let y = 0; y < r.content.length; y++) {
                  //
                  if (c.answers[x] == r.content[y]) {
                    existed = true;
                  }
                }
                if (!existed) {
                  //
                  right = false;
                  break;
                }
              }
            }
          }
        }
        obj.right = right;
        detail.push(obj);
        if (right) {
          score++;
        }
      }
      context.commit("setScore", score);
      let post = {
        user: context.state.userName,
        score: score,
        detail: detail
      }
      axios.post("./backend/", post)
        .then(response => window.console.log(response))
        .catch(error => window.console.log(error));
      window.console.log(post);
    }
  }
})
