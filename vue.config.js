const webpack = require("webpack");

module.exports = {
   configureWebpack: {
      devtool: 'source-map',
      plugins: [
         new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default']
         })
      ]
   },
   publicPath: "./",
   devServer: {
      // 设置主机地址
      host: 'localhost',
      // 设置默认端口
      port: 8080,
      // 设置代理
      proxy: {
         '/backend': {
            // 目标 API 地址
            // target: 'http://localhost/project3/backend/',
            target: 'http://192.168.9.15/cdapp/func/tkp-lui/backend/',
            // 如果要代理 websockets
            //ws: true,
            // 将主机标头的原点更改为目标URL
            changeOrigin: false,
            pathRewrite: {
               '^/backend': ''
            }
         }
      }
   }
}